<?php
require_once('/var/www/html/lib/Sanitiser.php');

class ROWSanitiser implements Sanitiser {

    public function sanitise($rawData) {
        $sanitised = [];
        foreach($rawData as $k=>$v) {
            switch($k) {
                case "layer":
                    $lyrs = explode(",", $rawData["layer"]);
                    $sanitised["layer"] = [];
                    foreach($lyrs as $lyr=>$v) {
                        if(ctype_alnum($v)) {
                            $sanitised["layer"][] = $v;
                        }
                    }
                    break;
                case "bbox":
                    $data = explode(",", $v);
                    if(count($data)==4) {
                        $ok = true;
                        for($i=0; $i<4; $i++) {
                            if(!preg_match("/^-?[\d\.]+$/",$data[$i])) {
                                $ok = false;
                                break;
                            } 
                        }
                        if($ok) {
                            $sanitised["bbox"] = $data;
                        }
                    }
                    break;
            }
        }
        return $sanitised;
    }
}
?>
