<?php

class Tile  {

    private $scale; // mpp
    private $tileSize;

    public const DEFAULT_SCALE = 5.0; // metres per pixel

    public function __construct ($tileSize) {
        $this->tileSize = $tileSize;
    }

    public function setParams($x, $y, $scale) {
        $this->tileX = $x;
        $this->tileY = $y;
        $this->ntiles = $scale / self::DEFAULT_SCALE;
        $this->im  = imagecreate($this->tileSize, $this->tileSize);
        $this->scale = $scale;
        imagecolorallocate($this->im, 220, 220, 220);
    }

    public function renderTile() {
        $tx = $this->tileX * $this->ntiles; 
        $ty = $this->tileY * $this->ntiles;
        $step = $this->tileSize / $this->ntiles;
        if($this->ntiles==1) {
            $file = "/var/www/html/mtp/ostiles/0/$tx/$ty.png";
//            echo "File is $file<br />";
            $srcIm = imagecreatefrompng($file);
            imagecopy($this->im, $srcIm, 0, 0, 0, 0, $this->tileSize, $this->tileSize);
        }
        elseif($this->ntiles > 1) {
            $dstY = $this->tileSize - $step;
            for($y = $ty; $y<$ty+(int)$this->ntiles; $y++) {
                $dstX = 0;
                for($x = $tx; $x<$tx+(int)$this->ntiles; $x++) {
                    $file = "/var/www/html/mtp/ostiles/0/$x/$y.png";
                    if(file_exists($file)) {
                        $srcIm = imagecreatefrompng ($file);
                        imagecopyresized($this->im, $srcIm, $dstX, $dstY, 0, 0, $step, $step, $this->tileSize, $this->tileSize);
                    }
                    $dstX += $step;
                }
                $dstY -= $step;
            }
        } else {
            $file = "/var/www/html/mtp/ostiles/0/".floor($tx)."/".floor($ty).".png";
//            echo "File is $file<br />";
            if(file_exists($file)) {
                $srcSize = $this->tileSize * $this->ntiles;
                $srcX = ($tx-floor($tx)) * $this->tileSize;
//                echo "tx $tx ty $ty<br/>";
                
                $srcY = ((floor($ty+1) - $ty) * $this->tileSize) - $this->tileSize*$this->ntiles;
//                echo "srcX $srcX srcY $srcY<br />";
                $srcIm = imagecreatefrompng($file);
                imagecopyresized($this->im, $srcIm, 0, 0, $srcX, $srcY, $step, $step, $this->tileSize, $this->tileSize);
            }
        }
    }

    public function outputPng() {
        imagepng($this->im);
        imagedestroy($this->im);
    }
}

?>
