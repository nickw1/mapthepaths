<?php

require('vendor/autoload.php');
require('controllers/TileController.php');
require('controllers/ROWController.php');
require('controllers/OsmAuthController.php');
require('controllers/OsmEditController.php');

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Container\ContainerInterface;

use Slim\Factory\AppFactory;

session_start();

$container = new \DI\Container();
AppFactory::setContainer($container);

$app = AppFactory::create();
$app->addRoutingMiddleware();
$app->addErrorMiddleware(true, true, true);

$routeParser = $app->getRouteCollector()->getRouteParser();

$container->set('view', function(ContainerInterface $c) {
    return new \Slim\Views\PhpRenderer("views");
});

$container->set('OsmAuthController', function(ContainerInterface $c) use($routeParser) {
    $view = $c->get('view');
    return new OsmAuthController($view, $routeParser);
});


$container->set('db', function(ContainerInterface $c) {
    return new PDO("pgsql:host=localhost;dbname=freemap", "gis");
});

$container->set('ROWController', function (ContainerInterface $c) {
    return new ROWController($c->get('db'));
});

$container->set('OsmEditController', function(ContainerInterface $c) {
    return new OsmEditController($c->get('db'));
});

$app->get('/', \OsmAuthController::class.":showPage")->setName("root");
$app->get('/tile/{z}/{x}/{y}.png', \TileController::class.':getTile');
$app->get('/row', \ROWController::class.':getROWs');
$app->get('/osm/login', \OsmAuthController::class.':login');
$app->get('/osm/logout', \OsmAuthController::class.':logout');
$app->post('/osm/edit', \OsmEditController::class.':edit');
$app->post('/osm/designation', \OsmEditController::class.':updateDes');

// hacks to avoid breaking app

$app->get('/row.php', \ROWController::class.':getROWs');

$app->run();


?>
