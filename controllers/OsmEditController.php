<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class OsmEditController {

    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function edit(Request $req, Response $res, array $args) {
        if(isset($_SESSION["access_token"]) && isset($_SESSION["access_secret"])) {
        $oauth = new OSMOAuthClient();
        $json = "";
        try {
            $json = $req->getParsedBody();
            $oauth->setSessionToken();
            $xml = "";

            if(!isset($json["changeset"]) || $json["changeset"]==0) {
                $xml =  "<osm> <changeset> <tag k=\"created_by\" v=\"MapThePaths\"/>";
                $xml = $xml . "<tag k=\"comment\" v=\"".($json["message"] ? $json["message"]: "MapThePaths update")."\"/> </changeset> </osm>";
                $result = $oauth->oauth->fetch("https://api.openstreetmap.org/api/0.6/changeset/create", $xml, OAUTH_HTTP_METHOD_PUT, ['Content-Type'=>'application/xml']);

                $changesetId = $oauth->oauth->getLastResponse();
            } else {
                $changesetId = $json["changeset"];
            }

            // json['way'] is now an array of ways, not just one way
            $ways = [];
            if(is_array($json['way'])) {
                  foreach($json['way'] as $way) {
                        $xml = "<osm><way changeset='$changesetId'".($way["id"] >=1 ? " id='{$way["id"]}' version='{$way["version"]}'" : "").">";
                        foreach($way['tags'] as $k=>$v) {
                            if(!empty($v) && $k !="osm_id") {
                                $xml .= "<tag k='$k' v='$v' />";
                            }
                        }
                        foreach ($way['nds'] as $nd) {
                            $xml .= "<nd ref='$nd' />";
                        }
                        $xml .="</way></osm>";
                        $result = $oauth->oauth->fetch("https://www.openstreetmap.org/api/0.6/way/".($way["id"]>=1 ? $way["id"]:"create"), $xml, OAUTH_HTTP_METHOD_PUT, ['Content-Type'=>'application/xml']);
                        if($result) {
                            $ways[] = $oauth->oauth->getLastResponse();
                        }
                    }
                }

                // $json['node'] is an array of node objects
                $nodes=[];
                if(is_array($json['node'])) {
                    foreach($json['node'] as $node) {
                        $xml = "<osm><node changeset='$changesetId' lat='$node[lat]' lon='$node[lon]'>";
                        foreach($node['tags'] as $k=>$v) {
                            $xml .= "<tag k='$k' v='$v' />";
                        }
                        $xml .= "</node></osm>";
                        $result = $oauth->oauth->fetch("https://www.openstreetmap.org/api/0.6/node/create", $xml, OAUTH_HTTP_METHOD_PUT, ['Content-Type'=>'application/xml']);
                        if($result) {
                        $nodes[] = $oauth->oauth->getLastResponse();
                        }
                    }
                }
            // Now send back JSON containing changeset and array of created node ids
                return $res->withJson(["changeset"=>$changesetId,"nodes"=>$nodes, "ways"=>$ways]);
            }catch(OAuthException $e) {
                return $res->withStatus($e->getCode())->withJson(["error" => $oauth->oauth->getLastResponse()]);
            }
        } else {
            return $res->withStatus(401)->withJson(["error" => "Session for OAuth token not set"]);
        }
    }

    public function updateDes(Request $req, Response $res, array $args) {
        $data = $req->getParsedBody(); 
        $modified=0;
        if(isset($data["ways"])) {
            foreach($data["ways"] as $way) {
                if(ctype_digit((string)$way["id"])) {
                    if(!isset($way["tags"]["highway"])) $way["tags"]["highway"] = null;
                    if(!isset($way["tags"]["designation"])) $way["tags"]["designation"] = null;
                    $stmt = $this->db->prepare("SELECT * FROM planet_osm_line WHERE osm_id=?");
                    $stmt->execute([$way["id"]]);
                    $row = $stmt->fetch();
    
                    $linestring = "LINESTRING(".implode(",",
                        array_map(function($val)  {
                            return "$val[lng] $val[lat]"; 
                        }, 
                        $way["coords"])).
                        ")";        
                    if($row) {
                        $stmt = $this->db->prepare("UPDATE planet_osm_line SET way=ST_Transform(ST_GeomFromText(?, 4326), 3857) WHERE osm_id=?");
                        $stmt->execute([$linestring, $way["id"]]);
                    } else {
                        $stmt = $this->db->prepare("INSERT INTO planet_osm_line (osm_id, way, highway, designation) VALUES (?, ST_Transform(ST_GeomFromText(?, 4326), 3857),?,?)");
                        $stmt->execute([$way["id"], $linestring, $way["tags"]["highway"], $way["tags"]["designation"]]);
                        $stmt = $this->db->prepare("INSERT INTO localDesignations(osm_id, designation) VALUES (?, ?)");
                        $stmt->execute([$way["id"], $way["tags"]["designation"]]);
                    }
                }
            }
        } elseif(isset($data["designation"]) && preg_match("/^[\w_]+$/", $data["designation"]) && $data["designation"]!="none") {
            $stmt = $this->db->prepare("UPDATE planet_osm_line SET designation=? WHERE osm_id=?");
            $sql = "UPDATE planet_osm_line SET designation=$data[designation] WHERE osm_id=$data[id]";
            $stmt->execute([$data["designation"], $data["id"]]);
            $modified = $stmt->rowCount();
            
            if($modified == 1) {
                $stmt = $this->db->prepare("INSERT INTO localDesignations(osm_id, designation) VALUES (?, ?)");
                $stmt->execute([$data["id"], $data["designation"]]);
                    
            }
            $res->getBody()->write("$modified");
            return $res;
        } else {
            return $res->withStatus(400);
        }
    }
}

?>
