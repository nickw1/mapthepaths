<?php

require_once('classes/OSMOAuthClient.php');
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class OsmAuthController {

    protected $view, $routeParser;

    public function __construct($view, $routeParser) {
        $this->view = $view;
        $this->routeParser = $routeParser;
    }

    public function login(Request $req, Response $res, array $args) {
        try {
            $oauth = new OSMOAuthClient();
            $requestToken = $oauth->login();
            return $res->withRedirect("https://www.openstreetmap.org/oauth/authorize?oauth_token=".$requestToken["oauth_token"]);
        } catch(OAuthException $e) {
            $error = $e->lastResponse;
            return $this->view->render($res, "index.phtml", ["routeParser"=>$this->routeParser, "error"=>$error]);
        }
    }

    public function showPage(Request $req, Response $res, array $args) {
        $user = $error = false;
        $inputData = $req->getQueryParams();
        try {
            $oauth = new OSMOAuthClient();
             if (self::loggedIn()) {
                $oauth->setSessionToken();
                $user = $oauth->getUser();
            } elseif (self::isOauthCallback($inputData)) {
                $at = $oauth->getAccessToken($inputData["oauth_token"], $_SESSION["request_secret"]);
                unset($_SESSION["request_secret"]);
                $_SESSION["access_token"] = $at["oauth_token"];
                $_SESSION["access_secret"] = $at["oauth_token_secret"];
                $oauth->setToken($_SESSION["access_token"], $_SESSION["access_secret"]);
                $user = $oauth->getUser();
                $_SESSION["username"] = $user["display_name"]; 
                return $res->withRedirect($this->routeParser->urlFor('root')); // remove oauth token from url
            } 
        } catch(OAuthException $e) {
            $error = "OauthException: " . $e->lastResponse;
        }
        return $this->view->render($res, "index.phtml", ["routeParser"=>$this->routeParser, "error"=>$error, "user"=>$user["display_name"], "mode"=>isset($inputData['mode']) && $inputData['mode'] == 1 ? 1: 0]);
        
    }

    public function logout(Request $req, Response $res, array $args) {
        session_destroy();
        return $res->withRedirect($this->routeParser->urlFor('root'));
    }

    static function loggedin() {
        return isset($_SESSION["access_token"]) && isset($_SESSION["access_secret"]);
    }

    static function isOauthCallback($inputData)  {
        return isset($inputData["oauth_token"]) && isset($_SESSION["request_secret"]);
    }
    
    public function loginStatus(Request $req, Response $res, array $args) {
        return $res->withJSON(["username"=>isset($_SESSION["username"]) ? $_SESSION["username"]: null]);
    }
}
?>
