<?php
require_once('classes/ROWSanitiser.php');

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class ROWController {

    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function getROWs(Request $req, Response $res, array $args) {    
        $sanitiser = new ROWSanitiser();
        $input = $sanitiser->sanitise($req->getQueryParams());
        $json = [];
        if(isset($input["bbox"])) {
            $bbox = $input["bbox"];
            $result = $this->db->query("SELECT id,name,description,ST_AsGeoJSON(the_geom) FROM council WHERE ST_Intersects(the_geom, 'BOX3D($bbox[0] $bbox[1],$bbox[2] $bbox[3])'::box3d)");
            $rows = $result->fetchAll(PDO::FETCH_ASSOC);
            foreach($rows as $row) {
                $copyright = "This data was provided by rowmaps.com and is subject to copyright by the appropriate council. Please see http://rowmaps.com/jsons/".substr($row["name"],0,2)." for terms.";
                $f = [ "type"=>"Feature",
                    "_id" => [ '$oid' => $row["id"]],
                    "properties" => [ "Name"=>$row["name"], "Description"=>$row["description"], "copyright"=>$copyright],
                    "geometry" => json_decode($row["st_asgeojson"],true)
                ];
                $json[] = $f;
            }
            header("Content-Type: application/json");
            echo json_encode($json);        
            return $res;
        } else {
            return $res->withStatus(400);
        }
    }
}
?>
