<?php

require_once('classes/Tile.php');

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class TileController {
    function getTile(Request $req, Response $res, array $args) {
        if(ctype_digit($args['x']) && ctype_digit($args['y']) && ctype_digit($args['z'])) {
            header("Content-type: image/png");
            $scales = [ 10, 5, 2.5, 1.25, 0.625, 0.3125 ];
            $t = new Tile(400);
            $t->setParams ($args['x'], $args['y'], $scales[$args['z']]);
            $t->renderTile();
            $t->outputPng();
            return $res;
        } else {
            return $res->withStatus(400);
        }
    }
}
?>
