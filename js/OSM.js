const Dialog = require('jsfreemaplib').Dialog;
const WaySplitter = require('./WaySplitter');

function OSM(options) {
    this.url = options.url || 'https://www.openstreetmap.org/api/0.6/map';
    this.findNearestCouncilWay = options.findNearestCouncilWay;
    this.onWayUpdate = options.onWayUpdate;
    this.nodes = {};
    this.ways =  {};
    this.nodeCircles = {};
    this.osmColours = options.osmColours;
    this.layer = new L.LayerGroup();
    this.nodeLayer = new L.LayerGroup();
    this.highlightElement = null;
    this.changeset = 0;
    this.newWayId = -1;
    this.editMode = 0;
    this.firstEdit = true;
    this.waySplitter = new WaySplitter ( this, this, { 
            updateWayOnOsm:    (nodeId, splitPoint, nodeIdx, colour, wayObj)=> {
                this.nodes[nodeId] = [splitPoint[1], splitPoint[0]];
                this.createNodeCircle(nodeId, nodeIdx, colour, wayObj.id);
                this.updateWayOnOsm(wayObj);
            },

            onWaySplit: (polyline, newWays, coords, colour)=> {
                this.ways[newWays[1].id] = newWays[1];
                this.ways[newWays[0].id] = newWays[0];
                this.createPolyline(newWays[0], coords[0], colour, true);                       
                this.createPolyline(newWays[1], coords[1], colour, true);
                polyline.removeFrom(this.layer); 
                let ndIdIdx = 0;
                newWays[1].nds.forEach (ndId => {
                    if(ndIdIdx > 0) {
                        delete this.nodeCircles[ndId].wayNodeIndices[newWays[0].id];
                    }
                    this.nodeCircles[ndId].wayNodeIndices[newWays[1].id] = ndIdIdx++;
                });
            },

            showMessage: msg=> {
                this.dlg.setContent(msg);
                this.dlg.show();
            }
        });
                        
    
    this.dlg = new Dialog('main', 
         {'OK' : ()=> {this.dlg.hide();} },
        { top: 'calc(50% - 100px)', left: 'calc(50% - 100px)', width: '300px',
             position: 'absolute',
            backgroundColor: 'white',
            padding: '5px',
            borderRadius: '5px',
            border: '1px solid black'});

    const modeIcons = Array.from(document.getElementById('mode').querySelectorAll("img"));
    modeIcons[this.editMode].classList.add('selected');
    for(let i=0; i<modeIcons.length; i++) {
        modeIcons[i].addEventListener("click", (function (i,e) {
            this.editMode = i;
            e.target.classList.add("selected");
            modeIcons.filter (icon => icon != e.target).forEach( icon=> {
                icon.classList.remove("selected");
            });
            if(this.editMode == 1) {
                this.layer.eachLayer (lyr => {
                    lyr.popupContent = lyr.getPopup().getContent();
                    lyr.unbindPopup();
                });
            } else {
                this.layer.eachLayer (lyr => {
                    if(lyr.popupContent) {
                        lyr.bindPopup(lyr.popupContent);
                        delete lyr.popupContent;
                    }
                });
            }
        }).bind(this,i));
    }
}

// NOTE OSM API seems to use CORS so we can call it via ajax directly
OSM.prototype.load = function(bbox) {
    var url = `${this.url}?bbox=${bbox.join(",")}`;
    fetch(url).then(response => response.text()).then(str => {
        var doc = new DOMParser().parseFromString(str, 'text/xml');
        var nodes = doc.documentElement.getElementsByTagName("node");
        for(var i=0; i<nodes.length; i++) {
            var nodeId = nodes[i].getAttribute('id');
            if(!this.nodes[nodeId]) {
                this.nodes[nodeId] = [parseFloat(nodes[i].getAttribute("lat")), parseFloat(nodes[i].getAttribute("lon"))];
            }
        }

        var ways = doc.getElementsByTagName("way");
        for(var i=0; i<ways.length; i++) {
            var wayObj = {};
            wayObj.tags = {};
            wayObj.nds = [];
            wayObj.id = ways[i].getAttribute("id");
            wayObj.version = ways[i].getAttribute("version");
            if(!this.ways[wayObj.id]) {
                var latlngs = [];
                for(var j=0; j<ways[i].getElementsByTagName("tag").length; j++) {
                    var t = ways[i].getElementsByTagName("tag")[j];
                    wayObj.tags[t.getAttribute("k")] = t.getAttribute("v");    
                }
                wayObj.tags.osm_id = wayObj.id;
                var colour = 'lightgray';
                if(wayObj.tags.highway) {
                    var designation = wayObj.tags.designation || 'none';
                    colour = this.osmColours[designation] || 'lightgray';
                    for(var j=0; j<ways[i].getElementsByTagName("nd").length; j++) {
                        var nd = ways[i].getElementsByTagName("nd")[j];
                        var lastNdId = 0;
                        var ndId = nd.getAttribute("ref");
                        wayObj.nds.push(ndId);
                        if(this.nodes[ndId]) {
                            if(wayObj.id == 763968175) {
                                console.log(`creating node circle ${ndId} ${j}`);
                            }
                            this.createNodeCircle(ndId, j, colour, wayObj.id);
                            latlngs.push(this.nodes[ndId]);
                            lastNdId= ndId;
                        }
                    } 
                    if(latlngs.length>=2) {
                        this.createPolyline(wayObj, latlngs, colour); 
                    }
                }
            }
        }
    }).catch(e => { console.log(e); } );
}

OSM.prototype.createNodeCircle = function(ndId, index, colour, wayId) {
    if(!this.nodeCircles[ndId]) {
        this.nodeCircles[ndId] = L.circle(this.nodes[ndId], { radius: 5, color: colour, fillColor: colour, pane: 'markerPane'}).addTo(this.nodeLayer);
          this.nodeCircles[ndId].wayNodeIndices = {};                      
        this.nodeCircles[ndId].on("click", e=> {
            if(this.highlightElement) {
                // If the node clicked (ndId) is within the selected way
                // and the index of ndId is the index we expect for this way
                const indexWithinHighlightedWay = this.highlightElement.wayObj.nds.indexOf(ndId);
                console.log(`indexWithinHighlightedWay=${indexWithinHighlightedWay} expected index=${this.nodeCircles[ndId].wayNodeIndices[this.highlightElement.wayObj.id]} way id=${this.highlightElement.wayObj.id} ndId=${ndId} nds=${JSON.stringify(this.highlightElement.wayObj.nds)} waynodeindices=${JSON.stringify(this.nodeCircles[ndId].wayNodeIndices)}`);
                if(indexWithinHighlightedWay == e.target.wayNodeIndices[this.highlightElement.wayObj.id]) {
                    this.waySplitter
                        .setPolyline(this.highlightElement)
                        .split(indexWithinHighlightedWay);
                } else {
                    this.dlg.setContent('This node is not on the selected way.');
                    this.dlg.show();
                }
            } else {
                this.dlg.setContent('Please select a way first.');
                this.dlg.show();
            }
          });
      } 
      this.nodeCircles[ndId].wayNodeIndices[wayId] = index;
}

OSM.prototype.createPolyline = function(wayObj, latlngs, colour, dbg=false) {
    var polyline = L.polyline(latlngs, {color: colour, opacity: 0.6, weight: 12.0, pane:'osmDataPane'});
    polyline.addTo(this.layer);
    polyline.osmId = wayObj.id;
    polyline.wayObj = wayObj;
    polyline.options.normalColour = colour;
    polyline.on("click", (function(polyline,wayObj,e) {
        if(this.editMode == 1) {
            if(e.target == this.highlightElement) {
                this.waySplitter
                    .setPolyline(polyline)
                    .insertNode(e.latlng);
            } else {
                this.dlg.setContent("Please select a way first; return to 'select' mode and ciick on the way to highlight it, then reselect 'split' mode and click on the way again to insert a new node into the way.");
                this.dlg.show();
            }
        } else {
             if(this.highlightElement) {
                this.highlightElement.setStyle({color:this.highlightElement.options.normalColour});
                this.highlightElement = null;
            }
            this.highlightElement = e.target;
            e.target.setStyle ( { color: '#ffff80' });
        }
            
    }).bind(this, polyline, wayObj));
    
    this.ways[wayObj.id] = polyline; 

    var cw = this.findNearestCouncilWay(polyline);

    if(cw.councilWay != null ) {
        this.bindPopupToWay(wayObj, polyline, cw.canEdit);
    }  else {
        /// !!! Without this you get the "Invalid L.LatLng(NaN, NaN)" error on click
        polyline.bindPopup('No nearby ROW found.');
    }
}

OSM.prototype.addTo = function(map) {
    this.layer.addTo(map);
    this.nodeLayer.addTo(map);
}

OSM.prototype.removeFrom = function(map) {
    this.nodeLayer.removeFrom(map);
    this.layer.removeFrom(map);
}


OSM.prototype.bindPopupToWay = function(way, polyline, canEdit) {
    var div = document.createElement('div');
    var h2 = document.createElement('h2');
    h2.appendChild(document.createTextNode('OSM live edit'));
    div.appendChild(h2);
    div.appendChild(document.createTextNode('ROW designation:'));
    var select = document.createElement('select');
    select.id = `designation${way.id}`;
        
    var none = document.createElement('option');
    none.value='';
    none.appendChild(document.createTextNode('--None--'));
    select.appendChild(none);
        
    for(let des in this.osmColours) {
        var option = document.createElement('option');
        option.value = des;
        option.selected = way.tags.designation && way.tags.designation==des;
        option.appendChild(document.createTextNode(des.replace(/_/g, ' ')));
        select.appendChild(option);
    }
    div.appendChild(select);    
    div.appendChild(document.createElement('br'));
    div.appendChild(document.createTextNode('ROW reference:'));
    var prowRefInput = document.createElement('input');
    prowRefInput.id=`prowRef${way.id}`;
    if(way.tags.prow_ref) {
        prowRefInput.value = way.tags.prow_ref;
    }
    div.appendChild(prowRefInput);

    var btn = document.createElement('input');
    btn.type='button';
    btn.value='Go!';
    
    btn.addEventListener('click', e=> {
        way.tags.designation = document.getElementById(`designation${way.id}`).value;
        var prowRef = document.getElementById(`prowRef${way.id}`).value;
        way.tags.prow_ref = prowRef;
        this.updateWayOnOsm(way);
    });
        
    div.appendChild(btn);
    polyline.bindPopup(canEdit ? div: 'Cannot edit as not in an OGL area');
}


OSM.prototype.updateWayOnOsm = function(way) {
    console.log(`updating way on OSM: new details=${JSON.stringify(way)}`);
    fetch('/osm/edit', {
        body: JSON.stringify({changeset: this.changeset, way:[way]}),
        headers: { 'content-type' : 'application/json' },
        credentials: 'include', // To pass the session cookie this MUST be included in Chrome. Firefox doesn't seem to need it.
        method: 'POST' } ).then
            (response =>  {
                if(response.status == 200) {
                    const colour = this.osmColours[way.tags.designation] || 'lightgray';
                    this.ways[way.id].setStyle ( { color:colour } ); 
                    this.ways[way.id].options.normalColour = colour;
                    this.highlightElement = null;
    
                    this.ways[way.id].redraw();
                    this.dlg.setContent(`success updating live OSM data: wayid=${way.id}`);
                    this.dlg.show();
                    response.json().then( json =>  { 
                        this.changeset = json.changeset; 
                        way.version = json.ways[0]; 
                     });

                    // fetch won't work with cors post reqests for some reason 
                    
                    fetch('/osm/designation', {
                        method: 'POST',
                        body: JSON.stringify({ id: way.id, designation: way.tags.designation}),
                        headers: {
                            'Content-Type' : 'application/json'
                        }
                    })
                    .then (response => {
                        if(response.status != 200) {
                            return Promise.reject(`Error with local update of designation. HTTP code=${response.status}`);
                        } else {
                            return response.text();
                        }
                    })
                    .then (nModified => {
                        if(nModified == "1") {
                            this.onWayUpdate(way.id, way.tags.designation);
                        }
                    })
                    .catch(e => {
                        this.dlg.setContent(e);
                        this.dlg.show();
                    })
                } else {
                    var t= "";
                    return Promise.reject(response.status=='401'? 'Your editing session has timed out, please log in again.\n' + err : `OpenStreetMap returned an error: code ${response.status}`);
                }
        } ).catch(e => { this.dlg.setContent(e); this.dlg.show(); });
}

OSM.prototype.showDialogOnFirstEdit = function() {
    if(this.firstEdit) {
        this.dlg.setContent("<h2>Editing instructions</h2><p>To edit a way's designation or ROW ref, click on it and fill in the details. To add a node to a way, click on the way to highlight it, then select 'insert node' mode (top right), then click on the way again. To split a way, select the way by clicking on it, then click on the node where you want to split it.</p>");
        this.dlg.show();
        this.firstEdit = false;
    }
};

module.exports = OSM;
