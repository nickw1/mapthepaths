
const turf = require('@turf/turf');
class WaySplitter {

    constructor(osm, newIdProvider, callbacks) {
        this.changesetProvider = osm;
        this.newIdProvider = newIdProvider;
        this.callbacks = callbacks;
    }

    setPolyline(polyline) {    
        this.polyline = polyline;
        return this;
    }

    insertNode(latlng) {

        this.callbacks.showMessage('Creating new node on OSM...');
        const latlngs = this.polyline.getLatLngs();
        const line = turf.lineString(latlngs.map (latLng => [latLng.lng, latLng.lat]));
        const point = turf.point([latlng.lng, latlng.lat]);
        // lineSlice INCLUDES the split point, snapped to line
        const lineBeforePoint = turf.lineSlice(line.geometry.coordinates[0], point, line);
        const splitPoint = lineBeforePoint.geometry.coordinates[lineBeforePoint.geometry.coordinates.length-1];
        const node = { lat: splitPoint[1], lon: splitPoint[0] , tags: {} };
            
        fetch('/osm/edit', {
                body: JSON.stringify({changeset: this.changesetProvider.changeset, node:[node]}),
                headers: { 'content-type' : 'application/json' },
                credentials: 'include', 
                method: 'POST' } )
                .then ( response => response.json() )
                .then ( json=> {
                    console.log(`Server returned: ${JSON.stringify(json)}`);
                    if(json.nodes.length == 1) {
                        this.changesetProvider.changeset = json.changeset;
                        this.polyline.wayObj.nds.splice(lineBeforePoint.geometry.coordinates.length-1, 0, json.nodes[0]);
                        this.polyline.getLatLngs().splice(lineBeforePoint.geometry.coordinates.length-1, 0, L.latLng([splitPoint[1], splitPoint[0]]));
                        this.callbacks.updateWayOnOsm(json.nodes[0], splitPoint, lineBeforePoint.geometry.coordinates.length-1, this.polyline.options.color, this.polyline.wayObj);
                        this.callbacks.showMessage('Node created successfully.');
                    }
                })
                .catch(e=> { this.callbacks.showMessage(`Error: ${e}`);  });
        
    }

    split(pos) {
        console.log(`Splitting ${this.polyline.wayObj.id} at pos ${pos}`);
        console.log(`Current way object: ${JSON.stringify(this.polyline.wayObj)}`);
        const newWays = this.splitWay(this.polyline.wayObj, pos); 
        console.log(`Result of split: ${JSON.stringify(newWays)}`);
        this.callbacks.showMessage('Splitting way...');
        fetch('/osm/edit', {
            body: JSON.stringify({changeset: this.changesetProvider.changeset, way:newWays}),
            headers: { 'content-type' : 'application/json' },
            credentials: 'include', 
            method: 'POST' } )
            .then ( response => response.json())
            .then(json => {
                newWays[1].id = json.ways[1];
                newWays[1].tags.osm_id = json.ways[1];
                newWays[0].version = json.ways[0];
                this.callbacks.onWaySplit(this.polyline, newWays, [
                        this.polyline.getLatLngs().slice(0, pos+1),
                        this.polyline.getLatLngs().slice(pos)
                    ], this.polyline.options.normalColour);
                

                this.highlightElement = null;
                this.callbacks.showMessage('Way split completed successfully.');
                    
                newWays[0].coords = this.polyline.getLatLngs().slice(0, pos+1);
                newWays[1].coords = this.polyline.getLatLngs().slice(pos);
    

                this.polyline = null;

                fetch('/osm/designation', {
                    method: 'POST',
                    body: JSON.stringify({ways: newWays}),
                    headers: {
                        'Content-Type' : 'application/json'
                    }
                })
                .then (response => {
                    if(response.status != 200) {
                        return Promise.reject(`Error with local update of ways. HTTP code=${response.status}`);
                    } else {
                        return response.text();
                    }
                })
                .catch(e => {
                    this.callbacks.showMessage(e);
                });
            });
    }

    splitWay (way, splitNodeIdx) {
        const newId = this.newIdProvider.newWayId--;
        const oldWay = { 
          nds: way.nds.slice(0, splitNodeIdx+1),
          id: way.id,
          tags: way.tags,
          version: way.version
        };
        const newWay = {
            nds: way.nds.slice(splitNodeIdx),
            id: newId, 
            tags: {},
            version: 1
        };
        Object.keys(way.tags).forEach ( tag => {
            newWay.tags[tag] = (tag=="osm_id" ? newId : way.tags[tag]);
        });

        return [oldWay, newWay];
    }
}

module.exports = WaySplitter;
