const Dialog = require('jsfreemaplib').Dialog;
const OSM = require('./OSM');

class MapThePaths {
  constructor() {
    var def='+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +ellps=airy +datum=OSGB36 +units=m +no_defs';
    this.crs = new L.Proj.CRS('EPSG:27700', def,
                 { bounds: L.bounds ([0,0],[700000,700000]),
                    resolutions: [10, 5, 2.5, 1.25, 0.625, 0.3125] } );
//    crs.scale = () => 0.2 // pixels per unit, i.e. metre 

    this.mode = 0;
    var lat = localStorage.getItem('lat') || 51.05; 
    var lon = localStorage.getItem('lon') || -0.72; 

    var pathTypeInfo = [ 
                { "osmName": "public_footpath",
                    "councilName": "Fo",
                    "readableName": "Public footpath" },
                { "osmName": "public_bridleway",
                    "councilName": "Br",
                    "readableName": "Public bridleway" },
                { "osmName": "restricted_byway",
                    "councilName": "Re",
                    "readableName": "Restricted byway" },
                { "osmName": "byway_open_to_all_traffic",
                    "councilName": "BO",
                    "readableName": "Byway open to all traffic" }
        ];


    if(window.location.search.length > 0) {
        var qsParams = window.location.search.substr(1).split('&');
        qsParams.forEach ( kv=> {
            var qsParam = kv.split('=');
            
            if(qsParam[0] == 'lat') {
                lat = qsParam[1];
            } else if (qsParam[0] == 'lon') {
                lon = qsParam[1];
            } else if (qsParam[0] == 'zoom') {
                zoom = qsParam[1];
            }  else if (qsParam[0] == 'mode') {
                this.mode = qsParam[1];
            }  
        });
    }
    
    this.map = (this.mode>0) ? L.map('map') : L.map('map', { crs: this.crs, renderer:L.canvas() } );
    this.indexedRenderedFeatures = { council: {} , osm: {} };
    this.loadedBounds = [];


    this.osmColours = {};
    var defaultOsmColours = [ '#008000', '#aa5500', '#800080', '#800000' ];
    for(var i=0; i<pathTypeInfo.length; i++) {
        this.osmColours[pathTypeInfo[i].osmName] = localStorage.getItem(`colour${i}`) ? tinycolor(localStorage.getItem(`colour${i}`)).darken(10).toString() : defaultOsmColours[i];
    }    
    this.councilColours = {};
    var defaultCouncilColours = [ '#00ff00', '#ff8000', '#ff00ff', '#ff0000' ];
    for(var i=0; i<pathTypeInfo.length; i++) {
        this.councilColours[pathTypeInfo[i].councilName] = localStorage.getItem(`colour${i}`) ? localStorage.getItem(`colour${i}`) : defaultCouncilColours[i];
    }    

    
    this.vmd = L.tileLayer ( 'tile/{z}/{x}/{y}.png', {  tms: true, maxZoom: 5, minZoom:0, tileSize:200, attribution: 'Map tiles &copy; Ordnance Survey, OS OpenData License; OSM data &copy; OpenStreetMap contributors, ODBL.', continuousWorld:true } );

    this.nlsgb1900 = L.tileLayer('https://nls-{s}.tileserver.com/bst0UQM5P0qQ/{z}/{x}/{y}.jpg', { minZoom: 1, maxZoom: 17, bounds: L.latLngBounds(new L.LatLng(49.852539,-7.7903077),new L.LatLng(60.894042,1.790425)),attribution:'<a href=\"http://maps.nls.uk/projects/subscription-api/\">National Library of Scotland</a>',opacity:0.85,subdomains:'0123'});
    this.nlsfirsted = L.tileLayer('https://nls-{s}.tileserver.com/bst0UQ5UMO0x/{z}/{x}/{y}.jpg', { minZoom: 1, maxZoom: 17, bounds: L.latLngBounds(new L.LatLng(49.852539,-7.7903077),new L.LatLng(60.894042,1.790425)),attribution:'<a href=\"http://maps.nls.uk/projects/subscription-api/\">National Library of Scotland</a>',opacity:0.85,subdomains:'0123'});

    if(this.mode==1) {
        this.nlsgb1900.addTo(this.map);
    } else {
        this.vmd.addTo(this.map);
    }

    var councilEraseColour = '#e0e0e0';
    var councilEraseWidth = 8;
    var osmHistoricalWidth = 2;

    
    this.map.on("baselayerchange", (e) => { 
        if(this.mode==1) {
            this.council.clearLayers();
            this.osm.clearLayers();
            this.indexedRenderedFeatures.council = {};
            this.indexedRenderedFeatures.osm = {};
            this.updateView();
        } 
    });
    

    var zoom = (this.mode>0) ? (localStorage.getItem('historicZoom') || 15) : (localStorage.getItem('zoom') || 1);
    this.doOsmEdit = false;

    var colourDlg = new Dialog('main', 
         {'OK': ()=> { 
            this.osm.clearLayers();
            this.council.clearLayers();
            this.indexedRenderedFeatures.council = {};
            this.indexedRenderedFeatures.osm = {};
            for(var i=0; i<4; i++) {
                var colour = document.getElementById(`colour${i}`).value;
                this.osmColours[pathTypeInfo[i].osmName] = tinycolor(colour).darken(10).toString();
                this.councilColours[pathTypeInfo[i].councilName] = colour;
                localStorage.setItem(`colour${i}`, colour);
            }
            colourDlg.hide(); 
        }, 'Cancel': ()=> { colourDlg.hide(); }},
        { top: '100px', left: '100px', width: '200px',
             position: 'absolute',
            fontSize: '80%',
            backgroundColor: 'white',
            padding: '5px',
            borderRadius: '5px',
            border: '1px solid black'});
    var div = document.createElement("div");
    var h2 = document.createElement("h2");
    h2.appendChild(document.createTextNode("Choose your colour scheme"));
    div.appendChild(h2);
    var types = [ "Footpath", "Bridleway", "Restricted byway", "Byway open to all traffic"];
    for(var i=0; i<4; i++) {
        var span = document.createElement("span");
        span.innerHTML = pathTypeInfo[i].readableName;
        div.appendChild(span);
        div.appendChild(document.createElement("br"));
        var input = document.createElement("input");
        input.id=`colour${i}`;
        input.setAttribute("type", "color");
        div.appendChild(input);
        div.appendChild(document.createElement("br"));
    }
    colourDlg.setDOMContent(div);

    document.getElementById('colourscheme').addEventListener('click', e=> {
        colourDlg.show();
        for(var i=0; i<pathTypeInfo.length; i++) {
            document.getElementById(`colour${i}`).value = this.councilColours[pathTypeInfo[i].councilName];
        }
    });

    this.map.setView([lat, lon], zoom);
    //map.setView([lat, lon], 1);

    var ogls = [ 'B1', 'BA', 'BB', 'BC', 'BI', 'BL', 'BO', 'CB', 'CN', 'CU', 'DY', 'DN', 'DT', 'EY', 'ES', 'EX', 'GR', 'HP', 'HD', 'HE', 'IW', 'KT', 'LA', 'L1', 'MA',  'NK', 'NY', 'OH', 'ON', 'PY', 'SC', 'SP', 'ST', 'YY', 'SK', 'SU', 'TR', 'WK', 'WN', 'WR', 'WO', 'YK', 'PW' ]; 

    var rowTypes = {};
    for(var i=0; i<pathTypeInfo.length; i++) {
        rowTypes[pathTypeInfo[i].councilName] = pathTypeInfo[i].readableName;
    }


    
    this.map.createPane('osmPane');
    this.map.getPane('osmPane').style.zIndex = 450;
    this.map.createPane('councilPane');
    this.map.getPane('councilPane').style.zIndex = 425;
    this.map.createPane('osmDataPane');
    this.map.getPane('osmDataPane').style.zIndex = 475;

    this.osm = L.geoJson( null,  {
        onEachFeature: (feature,layer) => {
            feature.properties.designation = feature.properties.designation||'none';
            feature.properties.prow_ref = feature.properties.prow_ref||'none';
            layer.feature = feature;
            this.indexedRenderedFeatures.osm[feature.properties.osm_id] = layer;
                    
        },
        style: feature => {
            return { color: this.osmColours[feature.properties.designation]|| (this.mode==1 && feature.properties.foot=='permissive' ? '#008080': '#a0a0a0'), opacity: 1.0, weight: this.mode==1 ? osmHistoricalWidth : 2, dashArray: this.osmColours[feature.properties.designation] ? null:(this.mode==1? null:[4,6]) };
        }, pane: 'osmPane' 
    });

    this.council = L.geoJson( null,  {
            onEachFeature: (feature,layer) => {
                var nameDetails = feature.properties.Name.split("|");
                feature.properties.county = nameDetails[0];
                feature.properties.parish = nameDetails[1];
                feature.properties.prow_ref = nameDetails[2];
                feature.properties.ogl = ogls.includes(feature.properties.county);
                    
                this.indexedRenderedFeatures.council[feature._id.$oid] = layer;
                layer.feature = feature;
                layer.bindPopup(`${rowTypes[feature.properties.designation]}, ref ${feature.properties.parish} ${feature.properties.prow_ref} <br />License:  ${feature.properties.ogl ? '<span class="yes">OGL (OSM compatible)</span>':'<span class="no">Not OSM compatible</span>'}`); }, 
            style: feature => {


                feature.properties.designation = feature.properties.Description.split("|")[0];
                return { color: this.mode==1? councilEraseColour : (this.councilColours[feature.properties.designation] || '#c0c0c0'), opacity: (this.mode == 1 ? 1:0.5), weight: (this.mode == 1? councilEraseWidth:6) };
            }, pane: 'councilPane'
    });
  
    this.council.name = 'council';
    this.osm.name = 'osm';
  
    this.osmedit = new OSM({ findNearestCouncilWay: this.findNearestCouncilWay.bind(this),         
                        onWayUpdate: (osmId,designation) => { 
                                if(this.indexedRenderedFeatures.osm[osmId]) {
                                    this.indexedRenderedFeatures.osm[osmId].setStyle ( { color:this.osmColours[designation] || 'gray', dashArray: this.osmColours[designation] ? null:[4,6] } ); 
                                    this.indexedRenderedFeatures.osm[osmId].redraw();
                                }
                            },
                        osmColours: this.osmColours } );

    this.council.addTo(this.map);
    this.osm.addTo(this.map);

    if(this.mode==1) {
        L.control.layers({ "NLS OS 6-inch-to-mile (1888-1913)":  this.nlsgb1900, "NLS OS 1:25000 1937-61 ": this.nlsfirsted}, {"Council ROWs":this.council, "OSM Footpaths": this.osm}).addTo(this.map);
    } else {
        L.control.layers({ "Ordnance Survey VectorMap District": this.vmd}, {"Council ROWs":this.council, "OSM Footpaths": this.osm}).addTo(this.map);
     } 
    this.map.on('dragend', this.updateView.bind(this)); 
    this.map.on('zoom', ()=> {
        if(document.getElementById('osmedit')) {
            this.fixEditShow();
            if(this.doOsmEdit) {
                console.log(this.map.getZoom());
                if(this.map.getZoom()>=3) {
                    this.osmedit.addTo(this.map);
                    this.osm.removeFrom(this.map);
                    document.getElementById('mode').style.display='block';
                } else {
                    this.osmedit.removeFrom(this.map);
                    this.osm.addTo(this.map);
                    document.getElementById('mode').style.display='none';
                }
            }
        }
        this.updateView();
    } ); 

    if(document.getElementById('osmedit')) {
        document.getElementById('osmedit').addEventListener("click", e=> {
                this.doOsmEdit = !this.doOsmEdit;
                if(this.doOsmEdit) {
                    this.osm.removeFrom(this.map);
                    this.osmedit.addTo(this.map);
                    document.getElementById('osmedit').innerHTML='Stop editing';
                    document.getElementById('mode').style.display = 'block';
                    this.fixEditShow();
                    this.updateView();
                    this.osmedit.showDialogOnFirstEdit();
                } else {
                    this.osmedit.removeFrom(this.map);
                    this.osm.addTo(this.map);
                    document.getElementById('osmedit').innerHTML='Edit';
                    document.getElementById('mode').style.display = 'none';
                    this.updateView();
                }
            });
        this.fixEditShow();
    }



    var searchDlg = new Dialog('main', 
                     {'OK': ()=> { searchDlg.hide(); }},
                    { top: '100px', left: '100px', width: '200px',
                     position: 'absolute',
                        fontSize: '80%',
                    backgroundColor: 'white',
                    padding: '5px',
                    borderRadius: '5px',
                    border: '1px solid black'});
    document.getElementById('searchBtn').addEventListener('click', e=> {
            var q = document.getElementById('q').value;
            fetch(`/nomproxy.php?q=${q},UK`).then(response=>response.json()).then(json=> {
                var nodes = json.filter(o => o.osm_type=='node');
                if(nodes.length==0) {
                    alert(`No results for '${q}'`);
                } else {
                    searchDlg.show();
                    var div = document.createElement("div");
                    var h2=document.createElement("h2");
                    h2.appendChild(document.createTextNode("Search results"));
                    div.appendChild(h2);
                    nodes.forEach(o=> {
                            var p = document.createElement("p");
                            var a = document.createElement("a");
                            a.href='#';
                            a.innerHTML = o.display_name;
                            a.addEventListener("click", e=> {
                                this.map.setView([o.lat, o.lon], 1);
                                searchDlg.hide();
                                this.updateView();
                            });
                            p.appendChild(a);
                            div.appendChild(p);
                        } );
                    searchDlg.setDOMContent(div);
                }
            } );    
        } );

    this.updateView();
    window.dispatchEvent(new Event("resize"));
  }

  findNearestCouncilWay (osmPolyline) {
        var obounds = osmPolyline.getBounds();
        var threshold = 10;
        var nOgls = 0, nNotOgls = 0, nContainingOrClosePts, maxSoFar = 0;
        var councilWay = null;
        var found = false;
        this.council.eachLayer ( councilPolyline => {
            nContainingOrClosePts = 0;
            found  = false;
            if(councilPolyline.getBounds().intersects(obounds)) {
                councilPolyline.getLatLngs().forEach ( councilPt => {
                    if(obounds.contains(councilPt)) {
                        nContainingOrClosePts++;
                        found = true;
                    } 
                } );    
            } 
            if(!found) {
                osmPolyline.getLatLngs().forEach( osmPt=> {
                    councilPolyline.getLatLngs().forEach(councilPt => {
                        if(osmPt.distanceTo(councilPt) <= threshold) {
                            nContainingOrClosePts++;
                    }} );
                } );
            }
            if(nContainingOrClosePts > 0) {
                nOgls += councilPolyline.feature.properties.ogl ? 1:0;
                nNotOgls += !councilPolyline.feature.properties.ogl ? 1:0;
                if(nContainingOrClosePts > maxSoFar) {
                    maxSoFar = nContainingOrClosePts;
                    councilWay = councilPolyline.feature;
                }
            }
        } );

        return { councilWay: councilWay, canEdit: nOgls>0 && nNotOgls==0 };
    }

    updateView()  {
        localStorage.setItem('lat', this.map.getCenter().lat);
        localStorage.setItem('lon', this.map.getCenter().lng);
        if(this.mode>0) {
            localStorage.setItem('historicZoom', this.map.getZoom());
        } else {
            localStorage.setItem('zoom', this.map.getZoom());
        }
        document.getElementById('permalink').href=`?lat=${this.map.getCenter().lat}&lon=${this.map.getCenter().lng}&zoom=${this.map.getZoom()}&mode=${this.mode}`;
        var curBounds = this.map.getBounds();
        var intersectingBounds = [], loaded=false;
            
        this.loadedBounds.some( lb => {
            if(lb.contains(curBounds)) {
                loaded=true;
                return true;
            }
            else if (lb.intersects(curBounds)) {
                intersectingBounds.push(lb);
            }
            return false;
        });
            

        if(!loaded) {
            var sw = curBounds.getSouthWest(), ne=curBounds.getNorthEast();
                    
            [this.council, this.osm].forEach (lyr => {
                var url = (lyr==this.council ? `row?&bbox=${sw.lng},${sw.lat},${ne.lng},${ne.lat}`: `/fm/ws/bsvr.php?fullways=1&way=footpaths&inProj=4326&bbox=${sw.lng},${sw.lat},${ne.lng},${ne.lat}&dbname=freemap&format=json`);
                fetch(url).then( response => response.json()).
                   then( json=> {
                    // goes slow if too many layers
                
                    // if more than 2000 objects on an osm layer
                    // this impacts performance - filter to 
                    // only show designation tags    
                    // TODO this really needs to be done properly
                    // server side with db queries

                    this.doAddFeatures(lyr, lyr==this.osm ? json.features : json);
            
                    if(lyr==this.council && this.mode==0 && this.map.getZoom()>=3 && this.doOsmEdit) {
                           this.osmedit.load([sw.lng, sw.lat, ne.lng, ne.lat]);
                    }
                            
                } ).catch(err=> { /*alert(err);*/ } );
            } );
                
        
            intersectingBounds.forEach (ib => { 
                ib.extend(curBounds);    
             } );
        }
    }

    doAddFeatures(lyr,json) {
        if(lyr.name=='osm' && json.length > 2000) {
            this.msg('Warning! Too much OSM data, showing only routes with a designation');
            json=json.filter( o => o.properties.designation ? true:false);
        } else {
            this.msg('');
        }


        // 190918 latest firefox seems to lag much more if lots added
        // this was a temporary Firefox bug with 63; now gone away
        if(lyr.getLayers().length > 3000) {
            lyr.clearLayers();
            this.loadedBounds = [];
            this.indexedRenderedFeatures[lyr.name] = {};
        }

    
        json.filter( f => (lyr.name=='osm' ? this.indexedRenderedFeatures.osm[f.properties.osm_id] : this.indexedRenderedFeatures.council[f._id.$oid]) ? false:true)
            .forEach( f => { 
                lyr.addData(f);
        });
    }

    fixEditShow()  {
        document.getElementById('osmedit').style.display=this.map.getZoom()>=3 ? 'inline':'none';
        document.getElementById('needToZoom').style.display=this.map.getZoom()>=3 ? 'none': 'block';
    }

    msg(msg) {
        document.getElementById('msg').style.display = msg=='' ? 'none': 'block'
        document.getElementById('msg').innerHTML = msg;
    }
}

new MapThePaths();
